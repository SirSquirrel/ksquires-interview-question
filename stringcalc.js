//takes in a string of comma seperated numbers and returns the total value of those numbers
function stringCalc(inputString) {
    if (inputString === "") {
        return 0;
    }
    //by default delimiter is comma
    var multiDelim = [];
    var delim = ",";
    //check if they are using a custom delimeter
    if (inputString.search(/^\/\/.+\n/) > -1) {
        //find first newline
        var pos = inputString.search(/\n/);
        //delim = delimiter found
        delim = inputString.substring(2, pos);
        //search to see if there are multiple delimeters seperated by a comma
        if (delim.search(/[^,],[^,]/) > -1) {
            multiDelim = delim.split(",");
        }
        //cut the delim from the string
        inputString = inputString.slice(pos + 1, inputString.length);
    }
    
    //purge the newlines I assume given how the examples were structered this is what you wanted if you wanted newlines as another delimeter I would've done split(/[,\n]/)
    inputString = inputString.replace(/\n/g, '');
    //if multiple delims create the expression for them to be split by
    if(multiDelim.length >1)
    {
        var segments = [inputString];
        var m;
        for (m = 0;m<multiDelim.length;m++)
        {
            var s;
            var chunks =[]
            for(s = 0;s<segments.length;s++)
            {
                chunks = chunks.concat(segments[s].split(multiDelim[m]));
            }
            segments = chunks;
        }
        numbers = segments;
    }
    else
    {
        var numbers = inputString.split(delim);
    }
    var i;
    var total = 0;
    for (i = 0; i < numbers.length; i++) {
        //numbers larger than 1000 should be ignored for bonus 1
        if (parseInt(numbers[i]) >1000){
            continue
        }
        total = total + parseInt(numbers[i]);
        //in order to ensure - can still be used as a delimiter this check for negative numbers must take place after the split
        try {
            if (numbers[i].search(/^-\d/) > -1) {
                throw "cannot use negative numbers";
            }
        }
        catch (err) {
            console.log("cannot input negative numbers");
            return NaN;
        }
    }
    return total
}

//15
console.log(stringCalc("0,1,2,3,4,5"));
//25
console.log(stringCalc("10,1,2,3,4,5"));
//215
console.log(stringCalc("100,110,5"));
//0
console.log(stringCalc(""));
//7
console.log(stringCalc("7"));
//8
console.log(stringCalc("1,2,5"));
//100
console.log(stringCalc("1,2,3,4,1,2,3,4,1,2,3,4,1,2,3,4,1,2,3,4,1,2,3,4,1,2,3,4,1,2,3,4,1,2,3,4,1,2,3,4"));
//6
console.log(stringCalc("1\n,2,3"));
//17
console.log(stringCalc("1\n,2,3,\n7,4\n"));
//8
console.log(stringCalc("//;\n1;3;4"));
//8
console.log(stringCalc("//;\n1;\n3;\n4"));
//6
console.log(stringCalc("//$\n1$2$3"));
//13
console.log(stringCalc("//@\n2@3@8"));
//10
console.log(stringCalc("///\n2/3/5"));
//6
console.log(stringCalc("//,,,\n2,,,3,,,1"));
//11
console.log(stringCalc("//OOOHHH\n2OOOHHH3OOOHHH6"));
//throw error
console.log(stringCalc("0,-1,2,3,4,5"));
//throw error
console.log(stringCalc("//OOOHHH\n-2OOOHHH3OOOHHH6"));
//1999
console.log(stringCalc("999,1000"));
//300
console.log(stringCalc("100,200,100000000,1001"));
//6
console.log(stringCalc("//abcdefghijk\n2abcdefghijk3abcdefghijk1"));
//24
console.log(stringCalc("//$,@\n2@3@8$9$1@1"));
//30
console.log(stringCalc("//heyooo,/\n2/3heyooo8/9/1heyooo1/6"));
//30
console.log(stringCalc("//$,@@@,/\n2@@@3@@@8$9$1@@@1/6"));
